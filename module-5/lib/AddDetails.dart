import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'DetailsList.dart';

class AddDetails extends StatefulWidget {
  const AddDetails({Key? key}) : super(key: key);

  @override
  State<AddDetails> createState() => _AddDetailsState();
}

class _AddDetailsState extends State<AddDetails> {
  @override
  Widget build(BuildContext context) {
    TextEditingController enter = TextEditingController();
    TextEditingController enter1 = TextEditingController();
    Future _AddToCard() {
      final enter_ = enter.text;
      final enter1_ = enter1.text;
    

      final ref = FirebaseFirestore.instance.collection("enter").doc();
      return ref
          .set({"enter": enter_, "enter1": enter1_, "docId": ref.id})
          .then((value) => log("cellection edit"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: enter,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20)),
                      hintText: "Enter "),
                )),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: enter1,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20)),
                      hintText: "Enter "),
                )),
            ElevatedButton(
                onPressed: () {
                  _AddToCard();
                },
                child: const Text("Add To Card"))
          ],
        ),
    // DetailsList()
      ],
    );
  }
}
