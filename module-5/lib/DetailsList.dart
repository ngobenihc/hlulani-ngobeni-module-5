import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DetailsList extends StatefulWidget {
  const DetailsList({Key? key}) : super(key: key);

  @override
  State<DetailsList> createState() => _DetailsListState();
}

class _DetailsListState extends State<DetailsList> {
  final Stream<QuerySnapshot> myDetails =
      FirebaseFirestore.instance.collection("details").snapshots();
  @override
  Widget build(BuildContext context) {
        return StreamBuilder(
          stream: myDetails,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasError) {
              return const Text("Something went wrong!!");
            } else if (snapshot.connectionState == ConnectionState.waiting) {
              return const CircularProgressIndicator();
            } else if (snapshot.hasData) {
              return Row(
                children: [
                  Expanded(child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.height,
                    child: ListView(
                      children: 
                        snapshot.data!.docs.map((DocumentSnapshot document) {
                          Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
                          return ListTile(
                            title: Text(data["enter"]),
                            subtitle: Text(data["enter1"])
                          );
                        }).toList(),
                    ),
                    )

                  )
                ],
              );
            }else{
              return const Text("No Data Found");
            }
          },
          );
  }
}
