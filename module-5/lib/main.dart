import 'package:flutter/material.dart';
import 'screens/Login.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';

// const firebaseConfig = {
//   apiKey: "AIzaSyDKJh89_kz7kFvg5gcQpIPlXyqM9X-d9hg",
//   authDomain: "ngobenihc-1808b.firebaseapp.com",
//   projectId: "ngobenihc-1808b",
//   storageBucket: "ngobenihc-1808b.appspot.com",
//   messagingSenderId: "620612158931",
//   appId: "1:620612158931:web:2be253464031c62c2e9b26"
// };
Future main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
     options: const FirebaseOptions(
      apiKey: "AIzaSyDKJh89_kz7kFvg5gcQpIPlXyqM9X-d9hg",
      appId: "1:620612158931:web:2be253464031c62c2e9b26",
      messagingSenderId: "620612158931",
      projectId: "ngobenihc-1808b",));
 
  appStart(const theApp());
}

class theApp extends StatelessWidget {
  const theApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: const  SplashScreen(),
    );
    
  }
}
class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splash: Column (
          children: [
            Image.asset('lib/assets/IMG22.JPG'),
          ],
        ),
        splashIconSize: 5000,
        duration: 1000,
        backgroundColor: Colors.black,
        nextScreen: const LogInScreen()
      );
  }
}
