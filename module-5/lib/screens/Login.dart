import 'package:flutter/material.dart';
import 'package:module3/screens/Dashboard.dart';
import 'package:module3/screens/SignUpPage.dart';
import 'package:module3/Theme.dart';
import 'package:module3/widgets/LogIn.dart';
import 'package:module3/widgets/Button.dart';

class LogInScreen extends StatelessWidget {
  const LogInScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: defaultPadding,
        child: Column(
          children: [
            const SizedBox(
              height: 130,
            ),
            Text(
              'LogIn Page',
              style: titleText,
            ),
            const SizedBox(
              height: 7,
            ),
            
            const SizedBox(
              height: 15,
            ),
            const logIn(),
            const SizedBox(
              height: 30,
            ),
            const Text(
              'Forgot Password?',
              style: TextStyle(
                color: secondaryColor,
                fontSize: 14,
                decoration: TextDecoration.underline,
                decorationThickness: 1,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Dashboard(),
                  ),
                );
              },
              child: const PrimaryButton(
                buttonText: ('LogIn'),
              ),
              
            ),
            Row(
              children: [
                const SizedBox(
                  width: 15,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const SignUpScreen(),
                      ),
                    );
                  },
                  child: Text(
                    'SignUp',
                    style: textButton.copyWith(
                      decoration: TextDecoration.underline,
                      decorationThickness: 1,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        
      ),
    );
  }
}
